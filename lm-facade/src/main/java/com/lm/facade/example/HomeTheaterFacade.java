package com.lm.facade.example;

import com.lm.facade.ele.DVDPlayer;
import com.lm.facade.ele.SoundSystem;
import com.lm.facade.ele.TV;

// 外观类
public class HomeTheaterFacade {
    private TV tv;
    private SoundSystem soundSystem;
    private DVDPlayer dvdPlayer;

    public HomeTheaterFacade(TV tv, SoundSystem soundSystem, DVDPlayer dvdPlayer) {
        this.tv = tv;
        this.soundSystem = soundSystem;
        this.dvdPlayer = dvdPlayer;
    }

    public void watchMovie(String movie) {
        System.out.println("Setting up home theater...");
        tv.turnOn();
        soundSystem.turnOn();
        soundSystem.setVolume(20);
        dvdPlayer.turnOn();
        dvdPlayer.play(movie);
        System.out.println("Enjoy your movie!");
    }

    public void endMovie() {
        System.out.println("Shutting down home theater...");
        dvdPlayer.turnOff();
        soundSystem.turnOff();
        tv.turnOff();
    }
}
