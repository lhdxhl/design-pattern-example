package com.lm.facade.example;

import com.lm.facade.ele.DVDPlayer;
import com.lm.facade.ele.SoundSystem;
import com.lm.facade.ele.TV;

// 测试代码
public class FacadePatternExample {
    public static void main(String[] args) {
        TV tv = new TV();
        SoundSystem soundSystem = new SoundSystem();
        DVDPlayer dvdPlayer = new DVDPlayer();

        HomeTheaterFacade homeTheater = new HomeTheaterFacade(tv, soundSystem, dvdPlayer);

        homeTheater.watchMovie("Inception");
        System.out.println();
        homeTheater.endMovie();
    }
}
