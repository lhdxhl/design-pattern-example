package com.lm.facade.ele;

// 子系统 2：音响
public class SoundSystem {
    public void turnOn() {
        System.out.println("Sound system is turned on.");
    }

    public void setVolume(int level) {
        System.out.println("Sound system volume set to " + level);
    }

    public void turnOff() {
        System.out.println("Sound system is turned off.");
    }
}
