package com.lm.facade.ele;

// 子系统 3：DVD 播放器
public class DVDPlayer {
    public void turnOn() {
        System.out.println("DVD player is turned on.");
    }

    public void play(String movie) {
        System.out.println("Playing movie: " + movie);
    }

    public void turnOff() {
        System.out.println("DVD player is turned off.");
    }
}