package com.lm.facade.ele;

// 子系统 1：电视
public class TV {
    public void turnOn() {
        System.out.println("TV is turned on.");
    }

    public void turnOff() {
        System.out.println("TV is turned off.");
    }
}
