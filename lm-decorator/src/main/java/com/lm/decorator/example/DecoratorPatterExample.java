package com.lm.decorator.example;

import com.lm.decorator.ele.Coffee;
import com.lm.decorator.ele.MilkDecorator;
import com.lm.decorator.ele.SugarDecorator;
import com.lm.decorator.inter.Beverage;

public class DecoratorPatterExample {
    public static void main(String[] args) {
        // 基础咖啡
        Beverage coffee = new Coffee();
        System.out.println(coffee.getDescription() + " -> Cost: $" + coffee.getCost());

        // 加牛奶的咖啡
        coffee = new MilkDecorator(coffee);
        System.out.println(coffee.getDescription() + " -> Cost: $" + coffee.getCost());

        // 加牛奶和糖的咖啡
        coffee = new SugarDecorator(coffee);
        System.out.println(coffee.getDescription() + " -> Cost: $" + coffee.getCost());
    }
}
