package com.lm.decorator.inter;

// 定义饮料接口
public interface Beverage {
    String getDescription();
    double getCost();
}
