package com.lm.decorator.ele;

import com.lm.decorator.inter.Beverage;

// 牛奶装饰器
public class MilkDecorator extends BeverageDecorator {
    public MilkDecorator(Beverage beverage) {
        super(beverage);
    }

    @Override
    public String getDescription() {
        return beverage.getDescription() + ", Milk";
    }

    @Override
    public double getCost() {
        return beverage.getCost() + 1.5;
    }
}