package com.lm.decorator.ele;

import com.lm.decorator.inter.Beverage;

// 基础咖啡类
public class Coffee implements Beverage {
    @Override
    public String getDescription() {
        return "Basic Coffee";
    }

    @Override
    public double getCost() {
        return 5.0;
    }
}