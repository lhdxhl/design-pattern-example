package com.lm.decorator;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Decorator  是装饰器模式样例
 */
@SpringBootApplication
public class DecoratorApplication {
    public static void main(String[] args) {
        SpringApplication.run(DecoratorApplication.class, args);
    }
}
