# 设计模式样例

#### 介绍
在实际开发过程中，会使用到各种各样的设计模式。本示例展示如何在 Spring Boot 项目中使用不同的模式：
##### 创建型模式
- Singleton（单例模式）
- Factory（工厂模式）
- Builder（建造者模式）
- Prototype（原型模式）

##### 行为型模式
- Visitor（访问者模式）
- Template（模板方法模式）
- Strategy（策略模式）
- Observer（观察者模式）
- Iterator（迭代器模式）
- Chain（责任链模式）
- Command（命令模式）
- Memento（备忘录模式）
- State（状态模式）
- Mediator（中介者模式）
- Interpreter（解释器模式）

##### 结构型模式
- Adapter（适配器模式）
- Decorator（装饰器模式）
- Proxy（代理模式）
- Facade（外观模式）
- Bridge（桥接模式）
- Composite（组合模式）
- Flyweight（享元模式）
