package com.lm.adapter.example;

import com.lm.adapter.ele.LoggerAdapter;
import com.lm.adapter.ele.OldLogger;
import com.lm.adapter.inter.LogTarget;

public class AdapterPatternExample {
    public static void main(String[] args) {
        // 使用旧版日志系统
        OldLogger oldLogger = new OldLogger();

        // 使用适配器将其适配到新接口
        LogTarget logger = new LoggerAdapter(oldLogger);

        // 通过适配后的接口记录日志
        logger.log("This is a log message.");
    }
}
