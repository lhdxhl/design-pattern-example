package com.lm.adapter.inter;

// 定义统一的日志接口
public interface LogTarget {
    void log(String message);
}
