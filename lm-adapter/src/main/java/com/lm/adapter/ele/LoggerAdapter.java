package com.lm.adapter.ele;

import com.lm.adapter.inter.LogTarget;

// 适配器类，将 OldLogger 适配为 LogTarget
public class LoggerAdapter implements LogTarget {
    private OldLogger oldLogger;

    public LoggerAdapter(OldLogger oldLogger) {
        this.oldLogger = oldLogger;
    }

    @Override
    public void log(String message) {
        // 调用旧版日志系统的方法
        oldLogger.printLog("Formatted Log: " + message);
    }
}