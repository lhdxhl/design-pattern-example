package com.lm.singleton.lazy;

/**
 * 懒汉式单例模式在第一次调用 getInstance() 方法时才会创建单例对象，避免了资源浪费
 */
public class LazySingleton {
    private static LazySingleton instance;

    private LazySingleton() {}

    public static synchronized LazySingleton getInstance() {
        if (instance == null) {
            instance = new LazySingleton();
        }
        return instance;
    }
}
