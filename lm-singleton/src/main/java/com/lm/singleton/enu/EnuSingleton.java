package com.lm.singleton.enu;

/**
 * 使用枚举创建单例是一种简单且安全的方式，
 * 枚举保证了线程安全性和防止反序列化破坏单例的特性
 */
public enum EnuSingleton {
    INSTANCE;

    // 添加其他属性和方法
    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
