package com.lm.singleton.lock;

/**
 * 双重检查锁定是一种优化懒汉式单例模式的方法，
 * 通过在同步块内外进行两次检查来避免每次调用 getInstance() 方法都进行同步操作
 */
public class LockSingleton {
    private static volatile LockSingleton instance;

    private LockSingleton() {}

    public static LockSingleton getInstance() {
        if (instance == null) {
            synchronized (LockSingleton.class) {
                if (instance == null) {
                    instance = new LockSingleton();
                }
            }
        }
        return instance;
    }
}
