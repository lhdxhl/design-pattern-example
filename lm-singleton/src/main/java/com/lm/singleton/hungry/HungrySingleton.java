package com.lm.singleton.hungry;

/**
 * 饿汉式单例模式在类加载时就创建单例对象
 */
public class HungrySingleton {
    private static final HungrySingleton instance = new HungrySingleton();

    private HungrySingleton() {}

    public static HungrySingleton getInstance() {
        return instance;
    }
}