package com.lm.singleton.example;

import com.lm.singleton.enu.EnuSingleton;

public class SingletonExample {
    public static void main(String[] args) {
        // 获取单例对象
        EnuSingleton singleton = EnuSingleton.INSTANCE;

        // 设置和获取数据
        singleton.setData("Hello, Singleton!");
        System.out.println(singleton.getData()); // 输出：Hello, Singleton!
    }
}
