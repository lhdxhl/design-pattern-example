package com.lm.singleton;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Singleton  是单例模式样例
 */
@SpringBootApplication
public class SingletonApplication {
    public static void main(String[] args) {
        SpringApplication.run(SingletonApplication.class, args);
    }
}
