package com.lm.singleton.cla;

/**
 * 静态内部类是一种延迟加载的方式，
 * 它能够在需要时才加载单例类，避免了资源浪费和多线程安全问题
 */
public class ClaSingleton {
    private ClaSingleton() {}

    private static class SingletonHolder {
        private static final ClaSingleton instance = new ClaSingleton();
    }

    public static ClaSingleton getInstance() {
        return SingletonHolder.instance;
    }
}
