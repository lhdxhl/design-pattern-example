package com.lm.flyweight.ele;

import com.lm.flyweight.inter.Shape;

// 具体享元类
public class Circle implements Shape {
    private String color; // 内在状态

    public Circle(String color) {
        this.color = color;
    }

    @Override
    public void draw(String extrinsicState) {
        System.out.println("Drawing Circle: Color = " + color + ", Extrinsic State = " + extrinsicState);
    }
}

