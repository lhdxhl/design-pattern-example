package com.lm.flyweight.example;

import com.lm.flyweight.inter.Shape;

public class FlyweightPatternExample {
    public static void main(String[] args) {
        Shape circle1 = ShapeFactory.getCircle("Red");
        circle1.draw("Radius = 5");

        Shape circle2 = ShapeFactory.getCircle("Blue");
        circle2.draw("Radius = 10");

        Shape circle3 = ShapeFactory.getCircle("Red");
        circle3.draw("Radius = 15");

        Shape circle4 = ShapeFactory.getCircle("Blue");
        circle4.draw("Radius = 20");
    }
}
