package com.lm.flyweight.example;

// 享元工厂类
import com.lm.flyweight.ele.Circle;
import com.lm.flyweight.inter.Shape;

import java.util.HashMap;
import java.util.Map;

public class ShapeFactory {
    private static final Map<String, Shape> circleMap = new HashMap<>();

    public static Shape getCircle(String color) {
        Shape circle = circleMap.get(color);
        if (circle == null) {
            circle = new Circle(color);
            circleMap.put(color, circle);
            System.out.println("Creating new Circle of color: " + color);
        }
        return circle;
    }
}
