package com.lm.flyweight.inter;

// 抽象享元类
public interface Shape {
    void draw(String extrinsicState);
}
