package com.lm.flyweight;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Flyweight  是享元模式样例
 */
@SpringBootApplication
public class FlyweightApplication {
    public static void main(String[] args) {
        SpringApplication.run(FlyweightApplication.class, args);
    }
}
