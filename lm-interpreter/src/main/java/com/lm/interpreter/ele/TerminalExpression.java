package com.lm.interpreter.ele;

import com.lm.interpreter.inter.Expression;

// 终端解释器
public class TerminalExpression implements Expression {
    private String data;

    public TerminalExpression(String data) {
        this.data = data;
    }

    @Override
    public boolean interpret(String context) {
        return context.contains(data);
    }
}
