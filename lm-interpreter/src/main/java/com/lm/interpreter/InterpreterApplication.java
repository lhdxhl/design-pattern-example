package com.lm.interpreter;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Interpreter  是解释器模式样例
 */
@SpringBootApplication
public class InterpreterApplication {
    public static void main(String[] args) {
        SpringApplication.run(InterpreterApplication.class, args);
    }
}
