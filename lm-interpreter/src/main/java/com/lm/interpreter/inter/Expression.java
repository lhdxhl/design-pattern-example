package com.lm.interpreter.inter;

// 解释器接口
public interface Expression {
    boolean interpret(String context);
}
