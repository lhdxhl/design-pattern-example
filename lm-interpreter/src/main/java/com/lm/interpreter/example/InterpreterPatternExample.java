package com.lm.interpreter.example;

import com.lm.interpreter.ele.AndExpression;
import com.lm.interpreter.ele.OrExpression;
import com.lm.interpreter.ele.TerminalExpression;
import com.lm.interpreter.inter.Expression;

// 测试类
public class InterpreterPatternExample {
    public static void main(String[] args) {
        Expression john = new TerminalExpression("John");
        Expression jane = new TerminalExpression("Jane");
        Expression george = new TerminalExpression("George");

        // Or Expression
        Expression orExpression = new OrExpression(john, jane);

        // And Expression
        Expression andExpression = new AndExpression(john, george);

        System.out.println("John or Jane? " + orExpression.interpret("John"));
        System.out.println("John and George? " + andExpression.interpret("John George"));
    }
}
