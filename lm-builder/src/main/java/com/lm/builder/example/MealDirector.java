package com.lm.builder.example;

// 指挥者
public class MealDirector {
    private MealBuilder mealBuilder;

    public MealDirector(MealBuilder mealBuilder) {
        this.mealBuilder = mealBuilder;
    }

    public void constructMeal() {
        mealBuilder.buildMainItem();
        mealBuilder.buildDrink();
        mealBuilder.buildDessert();
    }

    public Meal getMeal() {
        return mealBuilder.getMeal();
    }
}
