package com.lm.builder.example;

// 测试类
public class BuilderPatternExample {
    public static void main(String[] args) {
        // 创建儿童套餐
        MealBuilder kidsMealBuilder = new KidsMealBuilder();
        MealDirector kidsMealDirector = new MealDirector(kidsMealBuilder);
        kidsMealDirector.constructMeal();
        Meal kidsMeal = kidsMealDirector.getMeal();
        System.out.println("Kids Meal: " + kidsMeal);

        // 创建健身餐
        MealBuilder fitnessMealBuilder = new FitnessMealBuilder();
        MealDirector fitnessMealDirector = new MealDirector(fitnessMealBuilder);
        fitnessMealDirector.constructMeal();
        Meal fitnessMeal = fitnessMealDirector.getMeal();
        System.out.println("Fitness Meal: " + fitnessMeal);
    }
}