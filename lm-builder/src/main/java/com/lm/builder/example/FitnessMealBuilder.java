package com.lm.builder.example;

// 具体建造者：健身餐
public class FitnessMealBuilder implements MealBuilder {
    private Meal meal = new Meal();

    @Override
    public void buildMainItem() {
        meal.setMainItem("Grilled Chicken");
    }

    @Override
    public void buildDrink() {
        meal.setDrink("Protein Shake");
    }

    @Override
    public void buildDessert() {
        meal.setDessert("Greek Yogurt");
    }

    @Override
    public Meal getMeal() {
        return meal;
    }
}