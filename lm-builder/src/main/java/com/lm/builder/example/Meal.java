package com.lm.builder.example;

// 产品类
public class Meal {
    private String mainItem; // 主食
    private String drink;    // 饮料
    private String dessert;  // 甜品

    // Getter 和 Setter
    public String getMainItem() {
        return mainItem;
    }

    public void setMainItem(String mainItem) {
        this.mainItem = mainItem;
    }

    public String getDrink() {
        return drink;
    }

    public void setDrink(String drink) {
        this.drink = drink;
    }

    public String getDessert() {
        return dessert;
    }

    public void setDessert(String dessert) {
        this.dessert = dessert;
    }

    @Override
    public String toString() {
        return "Meal [Main Item: " + mainItem + ", Drink: " + drink + ", Dessert: " + dessert + "]";
    }
}
