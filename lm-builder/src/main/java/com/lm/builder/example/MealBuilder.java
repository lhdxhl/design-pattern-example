package com.lm.builder.example;

// 抽象建造者
public interface MealBuilder {
    void buildMainItem();
    void buildDrink();
    void buildDessert();
    Meal getMeal();
}
