package com.lm.builder.example;

// 具体建造者：儿童套餐
public class KidsMealBuilder implements MealBuilder {
    private Meal meal = new Meal();

    @Override
    public void buildMainItem() {
        meal.setMainItem("Chicken Nuggets");
    }

    @Override
    public void buildDrink() {
        meal.setDrink("Apple Juice");
    }

    @Override
    public void buildDessert() {
        meal.setDessert("Ice Cream");
    }

    @Override
    public Meal getMeal() {
        return meal;
    }
}

