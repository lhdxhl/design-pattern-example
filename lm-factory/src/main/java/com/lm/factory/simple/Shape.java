package com.lm.factory.simple;

// 形状接口
public interface Shape {
    void draw();
}
