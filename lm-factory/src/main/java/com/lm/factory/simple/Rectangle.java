package com.lm.factory.simple;

// 矩形实现类
public class Rectangle implements Shape {
    public void draw() {
        System.out.println("Drawing a Rectangle");
    }
}
