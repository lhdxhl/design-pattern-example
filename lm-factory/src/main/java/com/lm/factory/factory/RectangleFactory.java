package com.lm.factory.factory;

// 矩形工厂
public class RectangleFactory implements ShapeFactory {
    public Shape createShape() {
        return new Rectangle();
    }
}
