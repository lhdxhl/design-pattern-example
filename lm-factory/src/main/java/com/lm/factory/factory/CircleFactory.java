package com.lm.factory.factory;

// 圆形工厂
public class CircleFactory implements ShapeFactory {
    public Shape createShape() {
        return new Circle();
    }
}
