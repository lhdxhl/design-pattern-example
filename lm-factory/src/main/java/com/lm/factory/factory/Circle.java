package com.lm.factory.factory;

// 圆形实现类
public class Circle implements Shape {
    public void draw() {
        System.out.println("Drawing a Circle");
    }
}
