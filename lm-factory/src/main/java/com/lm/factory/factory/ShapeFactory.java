package com.lm.factory.factory;

// 工厂接口
public interface ShapeFactory {
    Shape createShape();
}
