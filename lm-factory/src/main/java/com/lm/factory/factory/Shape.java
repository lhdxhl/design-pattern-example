package com.lm.factory.factory;

// 形状接口
public interface Shape {
    void draw();
}
