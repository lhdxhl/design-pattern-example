package com.lm.factory.abs;

// 测试类
public class AbstractFactoryExample {
    public static void main(String[] args) {
        AbstractFactory shapeFactory = new ShapeFactory();
        Shape circle = shapeFactory.getShape();
        circle.draw();

        AbstractFactory colorFactory = new ColorFactory();
        Color red = colorFactory.getColor();
        red.fill();
    }
}
