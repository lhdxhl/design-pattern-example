package com.lm.factory.abs;

// 形状接口
public interface Shape {
    void draw();
}
