package com.lm.factory.abs;

// 颜色工厂
public class ColorFactory implements AbstractFactory {
    public Shape getShape() {
        return null; // 不支持形状
    }

    public Color getColor() {
        return new Red();
    }
}