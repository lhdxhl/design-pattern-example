package com.lm.factory.abs;

// 红色实现类
public class Red implements Color {
    public void fill() {
        System.out.println("Filling with Red color");
    }
}