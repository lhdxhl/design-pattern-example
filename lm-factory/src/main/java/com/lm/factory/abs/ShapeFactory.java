package com.lm.factory.abs;

// 形状工厂
public class ShapeFactory implements AbstractFactory {
    public Shape getShape() {
        return new Circle();
    }

    public Color getColor() {
        return null; // 不支持颜色
    }
}
