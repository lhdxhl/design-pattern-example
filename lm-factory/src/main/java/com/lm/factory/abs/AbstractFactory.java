package com.lm.factory.abs;

// 抽象工厂
public interface AbstractFactory {
    Shape getShape();
    Color getColor();
}
