package com.lm.mediator.ele;

import com.lm.mediator.inter.ChatMediator;

import java.util.ArrayList;
import java.util.List;

// 具体中介者
public class ChatRoom implements ChatMediator {
    private List<User> users = new ArrayList<>();

    @Override
    public void addUser(User user) {
        users.add(user);
    }

    @Override
    public void sendMessage(String message, User sender) {
        for (User user : users) {
            if (user != sender) {
                user.receiveMessage(message);
            }
        }
    }
}