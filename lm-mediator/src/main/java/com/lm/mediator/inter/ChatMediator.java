package com.lm.mediator.inter;

import com.lm.mediator.ele.User;

// 中介者接口
public interface ChatMediator {
    void sendMessage(String message, User user);
    void addUser(User user);
}
