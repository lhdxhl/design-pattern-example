package com.lm.mediator.example;

import com.lm.mediator.ele.ChatRoom;
import com.lm.mediator.ele.ChatUser;
import com.lm.mediator.ele.User;
import com.lm.mediator.inter.ChatMediator;

// 测试类
public class MediatorPatternExample {
    public static void main(String[] args) {
        ChatMediator chatRoom = new ChatRoom();

        User user1 = new ChatUser(chatRoom, "Alice");
        User user2 = new ChatUser(chatRoom, "Bob");
        User user3 = new ChatUser(chatRoom, "Charlie");

        chatRoom.addUser(user1);
        chatRoom.addUser(user2);
        chatRoom.addUser(user3);

        user1.sendMessage("Hello, everyone!");
        user2.sendMessage("Hi Alice!");
    }
}
