package com.lm.mediator;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Mediator  是中介者模式样例
 */
@SpringBootApplication
public class MediatorApplication {
    public static void main(String[] args) {
        SpringApplication.run(MediatorApplication.class, args);
    }
}
