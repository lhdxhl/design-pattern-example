package com.lm.observer.example;

import com.lm.observer.ele.ComputerDisplay;
import com.lm.observer.ele.PhoneDisplay;
import com.lm.observer.ele.WeatherData;
import com.lm.observer.inter.Observer;

// 测试类
public class ObserverPatternExample {
    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData();

        // 注册观察者
        Observer phoneDisplay = new PhoneDisplay();
        Observer computerDisplay = new ComputerDisplay();
        weatherData.registerObserver(phoneDisplay);
        weatherData.registerObserver(computerDisplay);

        // 模拟天气数据变化
        weatherData.setMeasurements(25.0f, 65.0f, 1013.0f);
        weatherData.setMeasurements(28.0f, 70.0f, 1012.0f);
    }
}
