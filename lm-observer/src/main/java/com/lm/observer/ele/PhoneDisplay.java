package com.lm.observer.ele;

import com.lm.observer.inter.Observer;

// 具体观察者：手机显示
public class PhoneDisplay implements Observer {
    @Override
    public void update(float temperature, float humidity, float pressure) {
        System.out.println("手机显示: 温度: " + temperature + "℃, 湿度: " + humidity + "%, 压力: " + pressure + "hPa");
    }
}