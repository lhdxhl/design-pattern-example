package com.lm.iterator.example;

import com.lm.iterator.ele.Book;
import com.lm.iterator.ele.BookShelf;

public class IteratorPatternExample {
    public static void main(String[] args) {
        BookShelf bookShelf = new BookShelf();

        bookShelf.addBook(new Book("设计模式"));
        bookShelf.addBook(new Book("Java编程思想"));
        bookShelf.addBook(new Book("重构"));

        for (Book book : bookShelf) {
            System.out.println("书籍: " + book.getTitle());
        }
    }
}
