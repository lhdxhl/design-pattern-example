package com.lm.iterator.ele;

import java.util.Iterator;
import java.util.List;

// 书架迭代器（具体迭代器类）
public class BookShelfIterator implements Iterator<Book> {
    private List<Book> books;
    private int position;

    public BookShelfIterator(List<Book> books) {
        this.books = books;
        this.position = 0;
    }

    @Override
    public boolean hasNext() {
        return position < books.size();
    }

    @Override
    public Book next() {
        return books.get(position++);
    }
}