package com.lm.iterator.ele;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// 书架类（聚合类）
public class BookShelf implements Iterable<Book> {
    private List<Book> books;

    public BookShelf() {
        books = new ArrayList<>();
    }

    public void addBook(Book book) {
        books.add(book);
    }

    @Override
    public Iterator<Book> iterator() {
        return new BookShelfIterator(books);
    }
}