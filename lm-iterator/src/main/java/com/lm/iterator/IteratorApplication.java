package com.lm.iterator;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Iterator  是迭代器模式样例
 */
@SpringBootApplication
public class IteratorApplication {
    public static void main(String[] args) {
        SpringApplication.run(IteratorApplication.class, args);
    }
}
