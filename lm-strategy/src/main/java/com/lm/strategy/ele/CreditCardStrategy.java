package com.lm.strategy.ele;

import com.lm.strategy.inter.PaymentStrategy;

// 具体策略：信用卡支付
public class CreditCardStrategy implements PaymentStrategy {
    private String cardNumber;

    public CreditCardStrategy(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    @Override
    public void pay(int amount) {
        System.out.println("使用信用卡支付: " + amount + " 元，卡号: " + cardNumber);
    }
}