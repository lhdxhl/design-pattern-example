package com.lm.strategy.ele;

import com.lm.strategy.inter.PaymentStrategy;

// 具体策略：支付宝支付
public class AlipayStrategy implements PaymentStrategy {
    @Override
    public void pay(int amount) {
        System.out.println("使用支付宝支付: " + amount + " 元");
    }
}
