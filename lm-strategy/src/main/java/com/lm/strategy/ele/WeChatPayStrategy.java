package com.lm.strategy.ele;

import com.lm.strategy.inter.PaymentStrategy;

// 具体策略：微信支付
public class WeChatPayStrategy implements PaymentStrategy {
    @Override
    public void pay(int amount) {
        System.out.println("使用微信支付: " + amount + " 元");
    }
}
