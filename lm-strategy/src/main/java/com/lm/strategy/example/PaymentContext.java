package com.lm.strategy.example;

import com.lm.strategy.inter.PaymentStrategy;

// 上下文类
public class PaymentContext {
    private PaymentStrategy paymentStrategy;

    // 设置支付策略
    public void setPaymentStrategy(PaymentStrategy paymentStrategy) {
        this.paymentStrategy = paymentStrategy;
    }

    // 执行支付
    public void pay(int amount) {
        if (paymentStrategy == null) {
            System.out.println("请先设置支付方式！");
        } else {
            paymentStrategy.pay(amount);
        }
    }
}
