package com.lm.strategy.example;

import com.lm.strategy.ele.AlipayStrategy;
import com.lm.strategy.ele.CreditCardStrategy;
import com.lm.strategy.ele.WeChatPayStrategy;

// 测试类
public class StrategyPatternExample {
    public static void main(String[] args) {
        PaymentContext context = new PaymentContext();

        // 使用支付宝支付
        context.setPaymentStrategy(new AlipayStrategy());
        context.pay(500);

        // 使用微信支付
        context.setPaymentStrategy(new WeChatPayStrategy());
        context.pay(300);

        // 使用信用卡支付
        context.setPaymentStrategy(new CreditCardStrategy("1234-5678-9012-3456"));
        context.pay(800);
    }
}
