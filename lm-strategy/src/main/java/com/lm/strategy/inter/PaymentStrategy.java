package com.lm.strategy.inter;

// 抽象策略接口
public interface PaymentStrategy {
    void pay(int amount);
}
