package com.lm.visitor.example;

import com.lm.visitor.inter.Employee;
import com.lm.visitor.inter.Visitor;

import java.util.ArrayList;
import java.util.List;

// 对象结构
public class Company {
    private List<Employee> employees = new ArrayList<>();

    public void addEmployee(Employee employee) {
        employees.add(employee);
    }

    public void accept(Visitor visitor) {
        for (Employee employee : employees) {
            employee.accept(visitor);
        }
    }
}
