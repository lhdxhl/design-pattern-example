package com.lm.visitor.example;

import com.lm.visitor.ele.Developer;
import com.lm.visitor.ele.Manager;
import com.lm.visitor.ele.SalaryAdjustmentVisitor;

public class VisitorPatternExample {
    public static void main(String[] args) {
        // 创建员工
        Manager manager = new Manager("Alice", 5000);
        Developer developer = new Developer("Bob", 3000);

        // 创建公司
        Company company = new Company();
        company.addEmployee(manager);
        company.addEmployee(developer);

        // 创建访问者
        SalaryAdjustmentVisitor salaryAdjustmentVisitor = new SalaryAdjustmentVisitor();

        // 调整薪资
        company.accept(salaryAdjustmentVisitor);

        // 输出调整后薪资
        System.out.println(manager.getName() + "'s new salary: " + manager.getSalary());
        System.out.println(developer.getName() + "'s new salary: " + developer.getSalary());
    }
}
