package com.lm.visitor.ele;

import com.lm.visitor.inter.Visitor;

// 具体访问者：薪资调整
public class SalaryAdjustmentVisitor implements Visitor {
    @Override
    public void visit(Manager manager) {
        System.out.println("Adjusting salary for Manager: " + manager.getName());
        manager.setSalary(manager.getSalary() * 1.10);
    }

    @Override
    public void visit(Developer developer) {
        System.out.println("Adjusting salary for Developer: " + developer.getName());
        developer.setSalary(developer.getSalary() * 1.20);
    }
}
