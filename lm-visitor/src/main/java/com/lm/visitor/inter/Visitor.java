package com.lm.visitor.inter;

import com.lm.visitor.ele.Developer;
import com.lm.visitor.ele.Manager;

// 访问者接口
public interface Visitor {
    void visit(Manager manager);
    void visit(Developer developer);
}
