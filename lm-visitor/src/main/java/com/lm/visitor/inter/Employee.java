package com.lm.visitor.inter;

public interface Employee {
    void accept(Visitor visitor);
    String getName();
    double getSalary();
    void setSalary(double salary);
}
