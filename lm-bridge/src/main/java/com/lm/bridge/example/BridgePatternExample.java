package com.lm.bridge.example;

import com.lm.bridge.ele.BlueColor;
import com.lm.bridge.ele.Circle;
import com.lm.bridge.ele.Rectangle;
import com.lm.bridge.ele.RedColor;
import com.lm.bridge.inter.Shape;

//测试
public class BridgePatternExample {
    public static void main(String[] args) {
        Shape redCircle = new Circle(10, new RedColor());
        Shape blueRectangle = new Rectangle(20, 15, new BlueColor());

        redCircle.draw();
        blueRectangle.draw();
    }
}
