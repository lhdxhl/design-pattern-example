package com.lm.bridge.ele;

import com.lm.bridge.inter.Color;
import com.lm.bridge.inter.Shape;

// 具体实现部分：圆形
public class Circle extends Shape {
    private int radius;

    public Circle(int radius, Color color) {
        super(color);
        this.radius = radius;
    }

    @Override
    public void draw() {
        System.out.print("Drawing a Circle with radius " + radius + " and color: ");
        color.applyColor();
    }
}