package com.lm.bridge.ele;

import com.lm.bridge.inter.Color;

public class BlueColor implements Color {
    @Override
    public void applyColor() {
        System.out.println("Blue");
    }
}
