package com.lm.bridge.ele;

import com.lm.bridge.inter.Color;

// 具体实现：红色
public class RedColor implements Color {
    @Override
    public void applyColor() {
        System.out.println("Red");
    }
}
