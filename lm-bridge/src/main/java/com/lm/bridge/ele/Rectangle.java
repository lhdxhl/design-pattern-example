package com.lm.bridge.ele;

import com.lm.bridge.inter.Color;
import com.lm.bridge.inter.Shape;

// 具体实现部分：矩形
public class Rectangle extends Shape {
    private int width, height;

    public Rectangle(int width, int height, Color color) {
        super(color);
        this.width = width;
        this.height = height;
    }

    @Override
    public void draw() {
        System.out.print("Drawing a Rectangle with width " + width + ", height " + height + " and color: ");
        color.applyColor();
    }
}