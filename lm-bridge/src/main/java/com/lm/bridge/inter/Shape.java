package com.lm.bridge.inter;

// 抽象部分：形状
public abstract class Shape {
    protected Color color; // 桥接接口

    public Shape(Color color) {
        this.color = color;
    }

    public abstract void draw();
}
