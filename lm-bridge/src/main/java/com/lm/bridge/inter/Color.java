package com.lm.bridge.inter;

// 实现部分：颜色接口
public interface Color {
    void applyColor();
}
