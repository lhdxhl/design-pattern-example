package com.lm.composite;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Composite  是组合模式样例
 */
@SpringBootApplication
public class CompositeApplication {
    public static void main(String[] args) {
        SpringApplication.run(CompositeApplication.class, args);
    }
}
