package com.lm.composite.ele;

import com.lm.composite.inter.Employee;

public class Manager implements Employee {
    private String name;
    private String position;

    public Manager(String name, String position) {
        this.name = name;
        this.position = position;
    }

    @Override
    public void showDetails() {
        System.out.println(name + " - " + position);
    }
}