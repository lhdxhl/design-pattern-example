package com.lm.composite.ele;

import com.lm.composite.inter.Employee;

// 叶子节点：普通员工
public class Developer implements Employee {
    private String name;
    private String position;

    public Developer(String name, String position) {
        this.name = name;
        this.position = position;
    }

    @Override
    public void showDetails() {
        System.out.println(name + " - " + position);
    }
}



