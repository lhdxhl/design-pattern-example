package com.lm.composite.example;

import com.lm.composite.ele.Department;
import com.lm.composite.ele.Developer;
import com.lm.composite.ele.Manager;
import com.lm.composite.inter.Employee;

public class CompositePatternExample {
    public static void main(String[] args) {
        Employee developer1 = new Developer("Alice", "Frontend Developer");
        Employee developer2 = new Developer("Bob", "Backend Developer");
        Employee manager1 = new Manager("Charlie", "Project Manager");

        Department engineering = new Department("Engineering");
        engineering.addEmployee(developer1);
        engineering.addEmployee(developer2);
        engineering.addEmployee(manager1);

        Employee hr1 = new Manager("Diana", "HR Manager");

        Department hr = new Department("Human Resources");
        hr.addEmployee(hr1);

        Department company = new Department("Company");
        company.addEmployee(engineering);
        company.addEmployee(hr);

        company.showDetails();
    }
}
