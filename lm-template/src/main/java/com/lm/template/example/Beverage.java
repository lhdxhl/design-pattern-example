package com.lm.template.example;
// 抽象类：饮料
public abstract class Beverage {
    // 模板方法
    public final void prepareRecipe() {
        boilWater();
        brew();
        pourInCup();
        addCondiments();
    }

    private void boilWater() {
        System.out.println("Boiling water");
    }

    private void pourInCup() {
        System.out.println("Pouring into cup");
    }

    // 抽象方法，子类实现
    protected abstract void brew();
    protected abstract void addCondiments();
}
