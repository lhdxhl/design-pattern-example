package com.lm.state.ele;

import com.lm.state.inter.OrderState;

public class CompletedState implements OrderState {
    @Override
    public void handleOrder() {
        System.out.println("订单状态：已完成。感谢您的购买！");
    }
}
