package com.lm.state.ele;

import com.lm.state.inter.OrderState;

public class PaidState implements OrderState {
    @Override
    public void handleOrder() {
        System.out.println("订单状态：已支付。订单处理中。");
    }
}
