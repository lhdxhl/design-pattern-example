package com.lm.state.ele;

import com.lm.state.inter.OrderState;

// 具体状态类
public class PendingState implements OrderState {
    @Override
    public void handleOrder() {
        System.out.println("订单状态：待支付。请完成支付！");
    }
}
