package com.lm.state;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * State  是状态模式样例
 */
@SpringBootApplication
public class StateApplication {
    public static void main(String[] args) {
        SpringApplication.run(StateApplication.class, args);
    }
}
