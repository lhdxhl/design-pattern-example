package com.lm.state.example;

import com.lm.state.inter.OrderState;

// 上下文类
public class OrderContext {
    private OrderState state;

    public void setState(OrderState state) {
        this.state = state;
    }

    public void processOrder() {
        if (state != null) {
            state.handleOrder();
        } else {
            System.out.println("订单状态未设置。");
        }
    }
}