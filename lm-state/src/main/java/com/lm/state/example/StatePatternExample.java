package com.lm.state.example;

import com.lm.state.ele.CompletedState;
import com.lm.state.ele.PaidState;
import com.lm.state.ele.PendingState;

// 测试类
public class StatePatternExample {
    public static void main(String[] args) {
        OrderContext order = new OrderContext();

        // 设置为待支付状态
        order.setState(new PendingState());
        order.processOrder();

        // 设置为已支付状态
        order.setState(new PaidState());
        order.processOrder();

        // 设置为已完成状态
        order.setState(new CompletedState());
        order.processOrder();
    }
}
