package com.lm.state.inter;

// 状态接口
public interface OrderState {
    void handleOrder();
}
