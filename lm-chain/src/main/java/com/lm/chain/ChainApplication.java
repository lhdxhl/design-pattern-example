package com.lm.chain;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Chain  是责任链模式样例
 */
@SpringBootApplication
public class ChainApplication {
    public static void main(String[] args) {
        SpringApplication.run(ChainApplication.class, args);
    }
}
