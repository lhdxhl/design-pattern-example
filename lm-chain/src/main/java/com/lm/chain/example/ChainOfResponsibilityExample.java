package com.lm.chain.example;

import com.lm.chain.ele.ConsoleLogger;
import com.lm.chain.ele.ErrorLogger;
import com.lm.chain.ele.FileLogger;
import com.lm.chain.ele.Logger;

public class ChainOfResponsibilityExample {
    private static Logger getChainOfLoggers() {
        Logger errorLogger = new ErrorLogger(Logger.ERROR);
        Logger fileLogger = new FileLogger(Logger.INFO);
        Logger consoleLogger = new ConsoleLogger(Logger.DEBUG);

        errorLogger.setNextLogger(fileLogger);
        fileLogger.setNextLogger(consoleLogger);

        return errorLogger;
    }

    public static void main(String[] args) {
        Logger loggerChain = getChainOfLoggers();

        loggerChain.logMessage(Logger.DEBUG, "This is a DEBUG level message.");
        loggerChain.logMessage(Logger.INFO, "This is an INFO level message.");
        loggerChain.logMessage(Logger.ERROR, "This is an ERROR level message.");
    }
}
