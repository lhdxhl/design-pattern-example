package com.lm.proxy.inter;

// 目标类
public class UserServiceImpl implements UserService {
    @Override
    public void addUser(String name) {
        System.out.println("User added: " + name);
    }
}
