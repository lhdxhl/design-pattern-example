package com.lm.proxy.cglibproxy;

public class ProductService {
    public void addProduct(String product) {
        System.out.println("Product added: " + product);
    }
}
