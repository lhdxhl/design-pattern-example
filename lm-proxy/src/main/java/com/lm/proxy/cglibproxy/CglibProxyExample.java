package com.lm.proxy.cglibproxy;

public class CglibProxyExample {
    public static void main(String[] args) {
        ProductService productService = (ProductService) ProductServiceCglibProxy.createProxy(new ProductService());
        productService.addProduct("Laptop");
    }
}
