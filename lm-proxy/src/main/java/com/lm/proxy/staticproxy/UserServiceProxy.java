package com.lm.proxy.staticproxy;

import com.lm.proxy.inter.UserService;

// 代理类
public class UserServiceProxy implements UserService {
    private final UserService userService;

    public UserServiceProxy(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void addUser(String name) {
        System.out.println("[LOG] Adding user...");
        userService.addUser(name);
        System.out.println("[LOG] User added.");
    }
}
