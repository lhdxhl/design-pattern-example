package com.lm.proxy.staticproxy;

import com.lm.proxy.inter.UserService;
import com.lm.proxy.inter.UserServiceImpl;

public class StaticProxyExample {
    public static void main(String[] args) {
        UserService userService = new UserServiceProxy(new UserServiceImpl());
        userService.addUser("Alice");
    }
}
