package com.lm.proxy.dynamicproxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

// 动态代理类
public class UserServiceDynamicProxy implements InvocationHandler {
    private final Object target;

    public UserServiceDynamicProxy(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("[LOG] Method " + method.getName() + " is called");
        Object result = method.invoke(target, args);
        System.out.println("[LOG] Method " + method.getName() + " execution completed");
        return result;
    }

    public static Object createProxy(Object target) {
        return Proxy.newProxyInstance(
                target.getClass().getClassLoader(),
                target.getClass().getInterfaces(),
                new UserServiceDynamicProxy(target)
        );
    }
}
