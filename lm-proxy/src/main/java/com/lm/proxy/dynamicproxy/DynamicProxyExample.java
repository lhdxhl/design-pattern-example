package com.lm.proxy.dynamicproxy;

import com.lm.proxy.inter.UserService;
import com.lm.proxy.inter.UserServiceImpl;

public class DynamicProxyExample {
    public static void main(String[] args) {
        UserService userService = (UserService) UserServiceDynamicProxy.createProxy(new UserServiceImpl());
        userService.addUser("Bob");
    }
}
