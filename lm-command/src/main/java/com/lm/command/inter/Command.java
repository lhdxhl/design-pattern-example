package com.lm.command.inter;

// 命令接口
public interface Command {
    void execute();
    void undo();
}
