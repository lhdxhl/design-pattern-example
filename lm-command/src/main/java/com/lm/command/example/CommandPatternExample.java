package com.lm.command.example;

import com.lm.command.ele.Light;
import com.lm.command.ele.LightOffCommand;
import com.lm.command.ele.LightOnCommand;
import com.lm.command.ele.RemoteControl;
import com.lm.command.inter.Command;

// 测试类
public class CommandPatternExample {
    public static void main(String[] args) {
        Light livingRoomLight = new Light();

        Command lightOn = new LightOnCommand(livingRoomLight);
        Command lightOff = new LightOffCommand(livingRoomLight);

        RemoteControl remote = new RemoteControl();

        // 开灯
        remote.setCommand(lightOn);
        remote.pressButton();

        // 撤销开灯
        remote.pressUndo();

        // 关灯
        remote.setCommand(lightOff);
        remote.pressButton();

        // 撤销关灯
        remote.pressUndo();
    }
}
