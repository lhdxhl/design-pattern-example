package com.lm.memento.example;

import com.lm.memento.ele.Caretaker;
import com.lm.memento.ele.TextEditor;

// 测试类
public class MementoPatternExample {
    public static void main(String[] args) {
        TextEditor editor = new TextEditor();
        Caretaker caretaker = new Caretaker();

        // 输入文本并保存状态
        editor.setText("Hello, World!");
        caretaker.save(editor.save());

        editor.setText("Hello, Design Patterns!");
        caretaker.save(editor.save());

        editor.setText("Hello, Memento Pattern!");

        System.out.println("当前文本: " + editor.getText());

        // 撤销一次
        editor.restore(caretaker.undo());
        System.out.println("撤销后文本: " + editor.getText());

        // 再撤销一次
        editor.restore(caretaker.undo());
        System.out.println("再次撤销后文本: " + editor.getText());
    }
}
