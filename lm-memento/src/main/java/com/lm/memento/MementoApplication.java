package com.lm.memento;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Memento  是备忘者模式样例
 */
@SpringBootApplication
public class MementoApplication {
    public static void main(String[] args) {
        SpringApplication.run(MementoApplication.class, args);
    }
}
