package com.lm.memento.ele;

import java.util.Stack;

// 负责人类
public class Caretaker {
    private final Stack<Memento> history = new Stack<>();

    public void save(Memento memento) {
        history.push(memento);
    }

    public Memento undo() {
        if (!history.isEmpty()) {
            return history.pop();
        }
        return null;
    }
}
