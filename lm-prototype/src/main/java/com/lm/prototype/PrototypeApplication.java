package com.lm.prototype;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *   Prototype 是原型模式样例
 */
@SpringBootApplication
public class PrototypeApplication {
    public static void main(String[] args) {
        SpringApplication.run(PrototypeApplication.class, args);
    }
}
