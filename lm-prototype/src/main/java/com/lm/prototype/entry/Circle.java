package com.lm.prototype.entry;

// 具体原型：圆形
public class Circle extends Shape {
    public Circle() {
        setType("Circle");
    }

    @Override
    public void draw() {
        System.out.println("Drawing a Circle");
    }
}
