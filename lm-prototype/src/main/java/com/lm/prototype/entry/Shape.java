package com.lm.prototype.entry;

import java.util.HashMap;
import java.util.Map;

// 抽象原型
public abstract class Shape implements Cloneable {
    private String id;
    private String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public abstract void draw();
}

