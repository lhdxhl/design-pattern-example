package com.lm.prototype.entry;

// 具体原型：矩形
public class Rectangle extends Shape {
    public Rectangle() {
        setType("Rectangle");
    }

    @Override
    public void draw() {
        System.out.println("Drawing a Rectangle");
    }
}
